import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Interface prévoyant la réception d'un flux, son traitement avant sa mise à disposition
 * @param <S> Objet en entrée
 * @param <M> Objet en sortie
 */
public interface ImageStreamingSerializer<S,M> {
	void serialize(S source, M media) throws IOException;
	<K extends InputStream> K getSourceInputStream(S source) throws IOException;
	<T extends OutputStream> T getSerializingStream(M media) throws IOException;

}
