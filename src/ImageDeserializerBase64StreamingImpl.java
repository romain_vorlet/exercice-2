import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;

/**
 * Classe permetttant le décodage du flux reçu en entrée et renvoyant le flux décodé dans un objet ByteArrayOutputStream
 */

public class ImageDeserializerBase64StreamingImpl implements ImageStreamingDeserializer<ImageByteArrayFrame> {

	private OutputStream boas ;

	public ImageDeserializerBase64StreamingImpl(ByteArrayOutputStream deserializationOutput) {
		this.boas=deserializationOutput;
	}

	// Décodage de l'objet reçu en entrée
	@Override
	public InputStream getDeserializingStream(ImageByteArrayFrame media) throws IOException {
		OutputStream out = media.getEncodedImageOutput();
		Base64OutputStream base64Decode = new Base64OutputStream(out, false);
		this.boas= base64Decode;
		media.setEncodedImageOutput(base64Decode);
		ByteArrayOutputStream temp = (ByteArrayOutputStream) boas;
		byte [] inputStream = temp.toByteArray();
		ByteArrayInputStream temp1 = new ByteArrayInputStream(inputStream);
		return temp1 ;
	}

	@SuppressWarnings("unchecked")
	@Override
	public OutputStream getSourceOutputStream() {
		return boas;
	}

	public void deserialize(ImageByteArrayFrame media) throws IOException {

	}
}
