import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Création de l'objet qui contiendra l'image encodée grâce au ByteArrayOutputStream reçu en paramètre
 */
public class ImageByteArrayFrame extends AbstractImageFrameMedia <ByteArrayOutputStream> {

	private OutputStream baos;
	
	public ImageByteArrayFrame(ByteArrayOutputStream byteArrayOutputStream) {
		// TODO Auto-generated constructor stub
		this.baos=byteArrayOutputStream;
	}

	public OutputStream getEncodedImageOutput() {
		// TODO Auto-generated method stub
		return baos;
	}

	/**
	 * Méthode inutilisée. La classe ne reçoit pas d'objet de type InputStream
	 */
	@Override
	public InputStream getEncodedImageInput() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	public OutputStream setEncodedImageOutput(OutputStream objetEcrire) {
		this.baos=objetEcrire;
		return baos;
	}

}
