import java.io.*;
import java.nio.file.Files;


import jdk.jshell.spi.ExecutionControl;
import org.apache.commons.codec.binary.Base64OutputStream;

/**
 * Classe permettant l'encodage en Base64 :
 * Un flux reçu en entrée est transmis à la méthode permettant l'encodage et la création d'un flux de sorti
 */
public class ImageSerializerBase64StreamingImpl extends AbstractStreamingImageSerializer <File, ImageByteArrayFrame>   {

	
	public ImageSerializerBase64StreamingImpl () {
		
	}
	@Override
	public FileInputStream getSourceInputStream(File source) throws IOException {
		FileInputStream fip = new FileInputStream(source);
		return fip;
	}
	@Override
	public OutputStream getSerializingStream(ImageByteArrayFrame media) throws IOException {
		OutputStream fluxSortant = media.getEncodedImageOutput();
		Base64OutputStream base64Encode = new Base64OutputStream(fluxSortant);
		
		//impression de la sérialisation
		System.out.println(splitDisplay(base64Encode.toString(),76));
		media.setEncodedImageOutput(base64Encode);
		return media.getEncodedImageOutput() ;
	}

	// méthode pour imprimer la sérialisation
	private static String splitDisplay(String str, int chars){
		StringBuffer strBuf = new StringBuffer();
		int i = 0;
		strBuf.append("================== Affichage de l'image encodée en Base64 ==================\n");
		for (; i+chars < str.length(); ){
			strBuf.append(str.substring(i,i+= chars));
			strBuf.append("\n");
		}
		strBuf.append(str.substring(i));
		strBuf.append("\n================================== FIN =====================================\n");

		return strBuf.toString();
	}

}

