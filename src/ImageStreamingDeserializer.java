import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Interface prévoyant le décodage d'un objet
 * @param <M> Objet reçu en paramètre
 */
public interface ImageStreamingDeserializer<M> {
	  void deserialize(M media) throws IOException;
	  <K extends InputStream> K getDeserializingStream(M media) throws IOException;
	  <T extends OutputStream> T getSourceOutputStream();
	}

