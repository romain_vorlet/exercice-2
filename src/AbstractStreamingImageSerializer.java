import java.io.IOException;

/**
 * Classe abstraite définissant une méthode non abstraite qui gère le communication entre les deux méthodes
 * getSourceInputStream et getSerializingStream
 * @param <S> Objet en entrée
 * @param <M> Objet en sortie
 */
public abstract class AbstractStreamingImageSerializer<S,M> implements ImageStreamingSerializer<S,M>  {
	  @Override
	  public final void serialize(S source, M media) throws IOException {
		  getSourceInputStream(source).transferTo(getSerializingStream(media));
	  
	  }

}
