import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class AbstractImageFrameMedia<T> {

	private T channel ;
	protected AbstractImageFrameMedia() {
    }

    // Accesseur de classe
    public T getChannel() {
        return channel;
    }

    // Mutateur de classe
    public void setChannel(T channel) {
        this.channel = channel;
    }

    // Constructeur de classe
    AbstractImageFrameMedia(T channel){
        this.channel = channel;
    }
    // Méthodew renvoyant la variable de classe, après son traitement, selont l'objet générique T défini
    public abstract OutputStream getEncodedImageOutput() throws IOException;

    public abstract InputStream getEncodedImageInput() throws IOException;
}
