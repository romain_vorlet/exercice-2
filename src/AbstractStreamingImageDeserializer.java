import java.io.IOException;

/**
 * Classe abstraite permettant le decryptage grace à un objet en entrée, et le renvoi du résultat
 * @param <M>
 */
public abstract class AbstractStreamingImageDeserializer<M> implements ImageStreamingDeserializer<M> {
    @Override
    public final void deserialize(M media) throws IOException {
    getDeserializingStream(media).transferTo(getSourceOutputStream());
    }

}
